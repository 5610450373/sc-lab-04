import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class showbox extends JFrame {
	private JLabel showProjectName;
	private JFrame frame;
	private JButton runbotton;
	private JTextField textin;
	private JComboBox comboBox;
	private JTextArea textout;
	private typebox box;

	public showbox(typebox box) {
		this.box = box;
		createframe();
	}
	public void createframe() {
		// TODO Auto-generated method stub
		showProjectName = new JLabel("Test");
		setLayout(null);
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Boxtype 1","Boxtype 2","Boxtype 3","Boxtype 4"}));
		comboBox.setBounds(50,40,150,40);
		add(comboBox);
		textin = new JTextField("5");
		textin.setBounds(50,110, 86, 20);
		add(textin);
		
		textout = new JTextArea();
		textout.setBounds(300, 20, 300, 340);
		add(textout);
		
		runbotton = new JButton("RUN");
		runbotton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String choice =(String) comboBox.getSelectedItem();
				int n = Integer.parseInt(textin.getText());
				if(choice=="Boxtype 1"){
					choice = box.type1(n);
				}
				if(choice=="Boxtype 2"){
					choice =box.type2(n);
				}
				if(choice=="Boxtype 3"){
					choice =box.type3(n);
				}
				if(choice=="Boxtype 4"){
					choice =box.type4(n);
				}
				textout.setText(choice);
			}
 
         
        });
		runbotton.setBounds(50, 150, 89, 23);
		add(runbotton);		
	}
	
		


}
